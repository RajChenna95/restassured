package restassured.day1;

import org.json.JSONException;
import org.json.JSONObject;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class UpdateProduct {

	public static void main(String[] args) throws JSONException {
		// TODO Auto-generated method stub
		System.out.println("Update Product API");
		RestAssured.baseURI="http://142.93.219.181:3000/api/";
		RequestSpecification rs=RestAssured.given();
		rs.header("Content-Type","application/JSON");
		rs.header("Authorization","eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJsYXN0X21vZGlmaWVkIjoxNTY0OTAwMDgzNjM5LCJleHBpcmVfYXQiOjE1NzAwODQwODM2NDAsInBheWxvYWQiOnsiaWQiOjIsIm5hbWUiOiJKYXRpbiBTaGFybWEiLCJlbWFpbCI6ImphdGludnNoYXJtYUBnbWFpbC5jb20iLCJyb2xlIjoiYWRtaW4iLCJtb2JpbGVfbnVtYmVyIjoiMTIzMTIzMTIzMSJ9fQ.ZlC45TGb2esY1U_nywdCxpUbEMD7eFVhdinVEfCfs_o");
		JSONObject JB=new JSONObject();
		JB.put("prod_name","Earphones");
		JB.put("prod_desc","Wired");
		JB.put("prod_price","1000");
		rs.body(JB.toString());
		Response response=rs.put("v1/products/152");
		System.out.println("Response is "+response.getStatusCode());
		System.out.println(response.asString());
	}

}
