package restassured.day1;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class SearchProduct {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
System.out.println("Search Product API");
RestAssured.baseURI="http://142.93.219.181:3000/api/";
RequestSpecification rs=RestAssured.given();
rs.header("Content-Type","application/JSON");
rs.header("Authorization","eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJsYXN0X21vZGlmaWVkIjoxNTY0OTAwMDgzNjM5LCJleHBpcmVfYXQiOjE1NzAwODQwODM2NDAsInBheWxvYWQiOnsiaWQiOjIsIm5hbWUiOiJKYXRpbiBTaGFybWEiLCJlbWFpbCI6ImphdGludnNoYXJtYUBnbWFpbC5jb20iLCJyb2xlIjoiYWRtaW4iLCJtb2JpbGVfbnVtYmVyIjoiMTIzMTIzMTIzMSJ9fQ.ZlC45TGb2esY1U_nywdCxpUbEMD7eFVhdinVEfCfs_o");
Response response=rs.get("v1/products/152");
System.out.println("Search API Response"+response.getStatusCode());
System.out.println(response.asString());




	}

}
