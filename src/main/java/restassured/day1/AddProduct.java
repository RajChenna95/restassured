package restassured.day1;

import org.json.JSONException;
import org.json.JSONObject;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class AddProduct {
	
	public static void main (String args[]) throws JSONException

	{
		
		System.out.println("Add Product API");
		RestAssured.baseURI="http://142.93.219.181:3000/api/";
		RequestSpecification rs=RestAssured.given();
		rs.header("Content-Type","application/JSON");
		rs.header("Authorization","eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJsYXN0X21vZGlmaWVkIjoxNTY0OTAwMDgzNjM5LCJleHBpcmVfYXQiOjE1NzAwODQwODM2NDAsInBheWxvYWQiOnsiaWQiOjIsIm5hbWUiOiJKYXRpbiBTaGFybWEiLCJlbWFpbCI6ImphdGludnNoYXJtYUBnbWFpbC5jb20iLCJyb2xlIjoiYWRtaW4iLCJtb2JpbGVfbnVtYmVyIjoiMTIzMTIzMTIzMSJ9fQ.ZlC45TGb2esY1U_nywdCxpUbEMD7eFVhdinVEfCfs_o");
		JSONObject JB=new JSONObject();
		JB.put("prod_name","Headset's");
		JB.put("prod_desc","Wireless");
		JB.put("prod_price","1099");
		rs.body(JB.toString());
		Response response=rs.post("v1/products/");
		System.out.println("Response is "+response.getStatusCode());
		System.out.println(response.asString());
		//Response response1=rs.get("v1/products/147");
		//System.out.println("Response is "+response1.getStatusCode());
		
		
		
		
		
	}

}
